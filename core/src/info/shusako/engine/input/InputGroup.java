package info.shusako.engine.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Shusako on 10/5/2016.
 * For project Voidwalker, 2016
 */
public class InputGroup {

    public InputGroup(String location) {
        Properties properties = new Properties();
        try {
            properties.load(Gdx.files.internal(location).read());
        } catch(IOException e) {
            e.printStackTrace();
        }

        inputMap = new HashMap<>();

        for(Object o : properties.keySet()) {
            String propertyName = (String) o;

            inputMap.put(propertyName, Input.Keys.valueOf(properties.getProperty(propertyName)));
        }
    }

    private Map<String, Integer> inputMap;

    public int getKeyForProperty(String property) {
        return inputMap.get(property);
    }
}

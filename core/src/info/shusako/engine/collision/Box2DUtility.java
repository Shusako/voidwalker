package info.shusako.engine.collision;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.world.WorldHandler;

/**
 * Created by Shusako on 12/1/2016.
 * For project Voidwalker, 2016
 */
public final class Box2DUtility {

    /**
     * Private constructor to prevent instantiation
     */
    private Box2DUtility() {

    }

    private static void createAbstractFixtureBox(Body body, String userData, float width, float height, Vector2
            center, float angle, float density, float friction, float restitution, boolean sensor, short
            categoryBits, short maskBits) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height, center, angle);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.isSensor = sensor;

        fixtureDef.filter.categoryBits = categoryBits;
        fixtureDef.filter.maskBits = maskBits;

        body.createFixture(fixtureDef).setUserData(userData);
        shape.dispose();
    }

    public static void createSensorBox(Body body, String userData, float width, float height, Vector2 center, float
            angle) {
        createAbstractFixtureBox(body, userData, width, height, center, angle, 0F, 0F, 0F, true, defaultBits,
                defaultBits);
    }

    public static void createSensorBox(Body body, String userData, float width, float height) {
        createAbstractFixtureBox(body, userData, width, height, new Vector2(0, 0), 0F, 0F, 0F, 0F, true, defaultBits,
                defaultBits);
    }

    public static void createCollisionBox(Body body, String userData, float width, float height, Vector2 center,
                                          float angle, float density, float friction, float restitution) {
        createCollisionBox(body, userData, width, height, center, angle, density, friction, restitution, defaultBits,
                defaultBits);
    }

    public static void createCollisionBox(Body body, String userData, float width, float height, Vector2 center,
                                          float angle, float density, float friction, float restitution, short
                                                  categoryBits, short maskBits) {
        createAbstractFixtureBox(body, userData, width, height, center, angle, density, friction, restitution, false,
                categoryBits, maskBits);
    }

    public static void createCollisionBox(Body body, String userData, float width, float height, float density, float
            friction, float restitution) {
        createCollisionBox(body, userData, width, height, new Vector2(0, 0), 0F, density, friction, restitution);
    }

    public static void createCollisionBox(Body body, String userData, float width, float height, float density, float
            friction, float restitution, short categoryBits, short maskBits) {
        createCollisionBox(body, userData, width, height, new Vector2(0, 0), 0F, density, friction, restitution,
                categoryBits, maskBits);
    }

    public static Body createBody(float x, float y, BodyDef.BodyType bodyType) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        bodyDef.position.set(x, y);

        Body body = WorldHandler.getHandler().getWorld().createBody(bodyDef);
        body.setFixedRotation(true);

        return body;
    }

    public static void addVelocity(Box2DComponent box2DComponent, float x, float y) {
        Vector2 velocity = box2DComponent.body.getLinearVelocity();
        velocity.x += x;
        velocity.y += y;
        box2DComponent.body.setLinearVelocity(velocity);
    }

    public static float getHeight(Box2DComponent box2DComponent) {
        Vector2 firstVertex = new Vector2();
        ((PolygonShape) box2DComponent.body.getFixtureList().get(0).getShape()).getVertex(0, firstVertex);
        // first vertex is negative half height from center
        return firstVertex.y * -2;
    }

    public static float getWidth(Box2DComponent box2DComponent) {
        Vector2 secondVertex = new Vector2();
        ((PolygonShape) box2DComponent.body.getFixtureList().get(0).getShape()).getVertex(1, secondVertex);

        return secondVertex.x;
    }

    private static final short defaultBits = (short) 0x1;
}

package info.shusako.engine.collision;

import com.badlogic.gdx.physics.box2d.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shusako on 11/29/2016.
 * For project Voidwalker, 2016
 */
public class CollisionListener implements ContactListener {

//    int collisionCount = 0;

    private List<CollisionCallback> callbacks = new ArrayList<>();

    /**
     * Registers a callback with the collision callback system.
     *
     * @param callback The callback
     */
    public void registerCollisionCallback(CollisionCallback callback) {
        callbacks.add(callback);
    }

    /**
     * De-registers a callback from the collision callback system.
     *
     * @param callback The callback
     */
    public void deregisterCollisionCallback(CollisionCallback callback) {
        callbacks.remove(callback);
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        String aUserData = (String) fixtureA.getUserData();
        String bUserData = (String) fixtureB.getUserData();

        if(aUserData == null && bUserData == null) {
            return;
        }

        for(CollisionCallback callback : callbacks) {
            if(callback.isConcernedWith(aUserData) || callback.isConcernedWith(bUserData)) {
                callback.beginContact();
            }
        }

//        if(fixtureA.getUserData() != null && fixtureA.getUserData().equals("onGroundSensor") || fixtureB.getUserData
//                () != null && fixtureB.getUserData().equals("onGroundSensor")) {
//            collisionCount++;
//        }
//
//        onGround = collisionCount != 0;
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        String aUserData = (String) fixtureA.getUserData();
        String bUserData = (String) fixtureB.getUserData();

        if(aUserData == null && bUserData == null) {
            return;
        }

        for(CollisionCallback callback : callbacks) {
            if(callback.isConcernedWith(aUserData) || callback.isConcernedWith(bUserData)) {
                callback.endContact();
            }
        }

//        if(fixtureA.getUserData() != null && fixtureA.getUserData().equals("onGroundSensor") || fixtureB.getUserData
//                () != null && fixtureB.getUserData().equals("onGroundSensor")) {
//            collisionCount--;
//        }
//
//        onGround = collisionCount != 0;
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

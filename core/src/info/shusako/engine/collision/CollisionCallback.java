package info.shusako.engine.collision;

/**
 * Created by Shusako on 11/29/2016.
 * For project Voidwalker, 2016
 */
public abstract class CollisionCallback {

    private String concernedUserData;

    protected CollisionCallback(String userData) {
        this.concernedUserData = userData;
    }

    boolean isConcernedWith(String userData) {
        return userData != null && userData.equals(concernedUserData);
    }

    /**
     * Will only be called when there's a collision that's started that involves some object with userData that the
     * callback is concerned with.
     */
    public abstract void beginContact();

    /**
     * Will only be called when a collision has ended that involves some object with userDatat that the callback is
     * concerned with.
     */
    public abstract void endContact();
}

package info.shusako.engine.collision;

/**
 * Created by Shusako on 1/21/2017.
 * For project Voidwalker, 2017
 */
public class CollisionCategory {

    /**
     * Collisions work by saying "I am this, I collide with this"
     * You can only be one thing.
     * You can collide with more than one thing.
     * <p>
     * If you say you are 0001 and collide with 0011 (0001 | 0010), you are saying that you collide with things of
     * similar type and with things of type 0010.
     * <p>
     * If you say you collide with 0000, that means you collide with nothing.
     */

    public static final short EVERYTHING = -1; // -1 is all 1's
    public static final short NOTHING = 0;
    public static final short WORLD = 1;
}

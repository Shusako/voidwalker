package info.shusako.engine.states;

/**
 * Created by Shusako on 1/28/2017.
 * For project Voidwalker, 2017
 */
public class States {

    /**
     * How many pixels per meter, to comply with Box2D conventions.
     * It's a float to avoid integer division when unexpected.
     */
    public static float PIXELS_PER_METER;
}

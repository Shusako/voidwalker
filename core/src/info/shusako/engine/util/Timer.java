package info.shusako.engine.util;

/**
 * Created by Shusako on 10/1/2016.
 * For project Voidwalker, 2016
 */
public class Timer {

    private long lastTick;
    private long deltaTime;

    public Timer(long deltaTime) {
        this.lastTick = System.currentTimeMillis();
        this.deltaTime = deltaTime;
    }

    public boolean hasTimePassed() {
        boolean timePassed = (System.currentTimeMillis() - lastTick) > deltaTime;
        if(timePassed)
            lastTick = System.currentTimeMillis();
        return timePassed;
    }
}

package info.shusako.engine.display;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import info.shusako.engine.display.states.CoordinateSystem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Shusako on 10/12/2016.
 * For project Voidwalker, 2016
 */
public class RenderableManager implements Renderable {

    private List<Renderable> renderableList;
    private static RenderableManager renderableManager = new RenderableManager();

    private RenderableManager() {
        renderableList = new ArrayList<>();
    }

    public static RenderableManager getInstance() {
        return renderableManager;
    }

    public void registerRenderable(Renderable renderable) {
        renderableList.add(renderable);
    }

    @Override
    public void render(SpriteBatch batch, Camera camera) {
//        batch.setProjectionMatrix(camera.combined.scl(1F / PIXELS_PER_METER));
//        batch.begin();

        Iterator<Renderable> iterator = renderableList.iterator();
        while(iterator.hasNext()) {
            Renderable renderable = iterator.next();
            if(renderable.expired()) {
                iterator.remove();
            } else {
                renderable.getCoordinateSystem().getCoordinateStateSwitcher().enable(batch, camera);
                renderable.render(batch, camera);
                renderable.getCoordinateSystem().getCoordinateStateSwitcher().disable(batch, camera);
            }
        }

//        batch.end();
//        batch.setProjectionMatrix(camera.combined.scl(PIXELS_PER_METER));
    }

    @Override
    public boolean expired() {
        System.err.println("RenderableManger expired function call. This is an error.");
        Thread.dumpStack();
        return false;
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        System.err.println("RenderableManager getCoordinateSystem function called. This is an error.");
        Thread.dumpStack();
        return null;
    }
}

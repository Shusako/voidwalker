package info.shusako.engine.display.states;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public interface CoordinateStateSwitcher {

    /**
     * Transforms the rendering properties to fit the current coordinate state, making any and all needed modifications
     * to the batch and camera.
     *
     * @param batch  The batch that is responsible for the rendering.
     * @param camera The current camera that is being rendered.
     */
    void enable(Batch batch, Camera camera);

    /**
     * Transforms the rendering properties back to a default states, void of direct affiliation with any coordinate
     * system through any and all modifications to the batch and camera.
     *
     * @param batch  The batch that is responsible for the rendering.
     * @param camera The current camera that is being rendered.
     */
    void disable(Batch batch, Camera camera);
}

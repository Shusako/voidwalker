package info.shusako.engine.display.states;

/**
 * This class is meant to be inherited by the client of the engine.
 * <p>
 * Recommended use of an enum file to implement this to store all the possible systems.
 * <p>
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public interface CoordinateSystem {

    /**
     * Gets the coordinate state switcher.
     *
     * @return The coordinate state switcher.
     */
    CoordinateStateSwitcher getCoordinateStateSwitcher();
}

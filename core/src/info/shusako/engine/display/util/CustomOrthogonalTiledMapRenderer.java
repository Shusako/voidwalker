package info.shusako.engine.display.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapImageLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import info.shusako.engine.entities.EntityRenderer;
import info.shusako.engine.loader.layer.EntityLayerLoader;

/**
 * Created by Shusako on 12/11/2016.
 * For project Voidwalker, 2016
 */
public class CustomOrthogonalTiledMapRenderer extends OrthogonalTiledMapRenderer {

    public CustomOrthogonalTiledMapRenderer(TiledMap map, float unitScale) {
        super(map, unitScale);
    }

    @Override
    protected void beginRender() {
        AnimatedTiledMapTile.updateAnimationBaseTime();
    }

    @Override
    protected void endRender() {}

    public void setBatch(Batch batch) {
        this.batch = batch;
    }

    public void render(Batch batch, OrthographicCamera camera, EntityRenderer entityRenderer) {
        this.setBatch(batch);
        this.setView(camera);

        beginRender();
        for(MapLayer layer : map.getLayers()) {

            String type = (String) layer.getProperties().get("type");
            if(type != null) {
                if(type.equalsIgnoreCase(EntityLayerLoader.layerType)) {
                    entityRenderer.render(batch);
                }
            }

            if(layer.isVisible()) {
                if(layer instanceof TiledMapTileLayer) {
                    renderTileLayer((TiledMapTileLayer) layer);
                }
                if(layer instanceof TiledMapImageLayer) {
                    renderImageLayer((TiledMapImageLayer) layer);
                } else {
                    renderObjects(layer);
                }
            }
        }
        endRender();
    }
}

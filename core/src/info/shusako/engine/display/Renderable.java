package info.shusako.engine.display;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import info.shusako.engine.display.states.CoordinateSystem;

/**
 * Created by Shusako on 10/12/2016.
 * For project Voidwalker, 2016
 */
public interface Renderable {

    /**
     * Render method that will be called on top of everything else
     *
     * @param batch  The current render batch
     * @param camera The camera that the batch should focus on rendering to
     *
     * @Precondition batch.isDrawing
     */
    void render(SpriteBatch batch, Camera camera);

    /**
     * If the renderable is expired, and if it is, it should be removed from the render list
     *
     * @return If the renderable should be removed from the render list
     */
    boolean expired();

    /**
     * The coordinate system that the renderable is assuming they are being rendered under. Choices are from
     * Renderable.CoordinateSystem.
     *
     * @return The coordinate system this renderable should be rendered in
     */
    CoordinateSystem getCoordinateSystem();
}

package info.shusako.engine.ai.camera;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import info.shusako.voidwalker.display.util.Styles;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.FollowingCameraComponent;
import info.shusako.voidwalker.util.MathUtil;

import static info.shusako.engine.states.States.PIXELS_PER_METER;
import static info.shusako.voidwalker.display.menus.InGame.SCREEN_WIDTH_IN_METERS_BASE;

/**
 * Created by Shusako on 9/17/2016.
 * Made for project Voidwalker
 */
public class DualFollowingCameraAI implements FollowingCameraKernel {

    private final int maxDistance = 35;
    private FollowingCameraAI primaryCamera;
    private FollowingCameraAI secondaryCamera;

    private boolean shouldRenderBothCameras;

    public DualFollowingCameraAI(Camera primaryCamera, Camera secondaryCamera) {
        this.primaryCamera = new FollowingCameraAI(primaryCamera);
        this.secondaryCamera = new FollowingCameraAI(secondaryCamera);

        this.primaryCamera.setMaximumDistance(maxDistance);
        this.secondaryCamera.setMaximumDistance(maxDistance);
    }

    private void updateShouldRenderBothCameras() {
        if(primaryCamera.getDefaultEntity() != null && secondaryCamera.getDefaultEntity() != null) {
            /*
             * If the two cameras are focused on entities of different priorities (under default priority category),
             * don't render both, we only care about the one with the highest priority, which will be selected by the
             * primary camera
             */
            if(FollowingCameraComponent.mapper.get(primaryCamera.getDefaultEntity()).priority >
                    FollowingCameraComponent.mapper.get(secondaryCamera.getDefaultEntity()).priority) {
                shouldRenderBothCameras = false;

                /*
                 * If the primary camera is focused on a super priority entity, and the secondary camera is not
                 * focused on that super priority entity, render both.
                 */
            } else if(primaryCamera.getFocusedEntity() != primaryCamera.getDefaultEntity() && secondaryCamera
                    .getFocusedEntity() != primaryCamera.getFocusedEntity()) {
                shouldRenderBothCameras = true;
            } else { // Fallback to determining it by distance.
                float distance = MathUtil.getDistance(primaryCamera.getFocusedEntity(), secondaryCamera
                        .getFocusedEntity());

                shouldRenderBothCameras = distance > maxDistance;
            }
        } else {
            // Only option at this point is to hope that it came here because there's no secondary target.
            shouldRenderBothCameras = false;
        }
//        shouldRenderBothCameras = true;
    }

    private void setMinimumDimensions(float v, float v1) {
        primaryCamera.setMinimumDimensions(v, v1);
        secondaryCamera.setMinimumDimensions(v, v1);
    }

    @Override
    public void update() {
        updateShouldRenderBothCameras();

        primaryCamera.update();
        secondaryCamera.getIgnoreEntityList().add(primaryCamera.getDefaultEntity());
//        secondaryCamera.getIgnoreEntityList().add(primaryCamera.getFocusedEntity());
        secondaryCamera.update();
        secondaryCamera.getIgnoreEntityList().remove(primaryCamera.getDefaultEntity());
//        secondaryCamera.getIgnoreEntityList().remove(primaryCamera.getFocusedEntity());
    }

    private float widthAnimation = 0;
    private static final float DELTA_WIDTH_ANIMATION = 2048F;

    @Override
    public void render(SpriteBatch batch, float delta, int x, int y, int width, int height) {
        float aspectRatio = width / (float) height;

        float newWidth = width - widthAnimation;
        float widthRatio = newWidth / width;

        this.setMinimumDimensions(SCREEN_WIDTH_IN_METERS_BASE * widthRatio, SCREEN_WIDTH_IN_METERS_BASE / aspectRatio);

        // left side
        primaryCamera.render(batch, delta, x, y, (int) newWidth, height);

        if(shouldRenderBothCameras) {
            drawArrows(batch, primaryCamera, secondaryCamera.getFocusedEntity());
            // right side
            secondaryCamera.render(batch, delta, x + (int) newWidth, y, (int) widthAnimation, height);
            drawArrows(batch, secondaryCamera, primaryCamera.getFocusedEntity());

            Gdx.gl.glViewport(x, y, width, height);
            drawSeparator(batch, height);

            widthAnimation += DELTA_WIDTH_ANIMATION * delta;
            if(widthAnimation > Gdx.graphics.getWidth() / 2)
                widthAnimation = Gdx.graphics.getWidth() / 2;
        } else {
            widthAnimation -= DELTA_WIDTH_ANIMATION * delta;
            if(widthAnimation < 0)
                widthAnimation = 0;
        }

        // reset the viewport to the entire screen
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void drawSeparator(SpriteBatch batch, int height) {
        float cameraX = secondaryCamera.getCamera().position.x;
        float cameraY = secondaryCamera.getCamera().position.y;

        float halfViewportHeight = secondaryCamera.getCamera().viewportHeight / 2;

        float scaleX = 0.5F;
        float textureWidth = Styles.separatorTexture.getWidth() / PIXELS_PER_METER;
        float textureHeight = Styles.separatorTexture.getHeight() / PIXELS_PER_METER;

        batch.begin();
        batch.draw(Styles.separatorTexture, cameraX, (cameraY - halfViewportHeight), -(textureWidth / 2), 0,
                textureWidth, height, scaleX, 1, 0, 0, 0, Styles.separatorTexture.getWidth(), Styles.separatorTexture
                        .getHeight(), false, false);
        batch.end();
    }

    // camera is world coords
    private void drawArrows(SpriteBatch batch, FollowingCameraAI camera, Entity entity) {
        // world coords
        Vector2 entityPosition = Box2DComponent.mapper.get(entity).body.getPosition();

        float cameraX = camera.getCamera().position.x;
        float cameraY = camera.getCamera().position.y;

        float halfViewportWidth = camera.getCamera().viewportWidth / 2;
        float halfViewportHeight = camera.getCamera().viewportHeight / 2;

        float slope = (entityPosition.y - cameraY) / (entityPosition.x - cameraX);
        int direction = entityPosition.x > cameraX ? 1 : -1;

        float theta = (float) Math.atan(slope);

        float changeFactor;
        {
            float thetaCritical = (float) Math.atan(halfViewportHeight / halfViewportWidth);
            if(Math.abs(theta) > thetaCritical) {
                changeFactor = (float) (halfViewportHeight / Math.sin(Math.abs(theta)));
            } else {
                changeFactor = (float) (halfViewportWidth / Math.cos(theta));
            }
        }

        changeFactor *= 0.9;
        float oscillationAmplitude = changeFactor * 0.02F;
        changeFactor += oscillationAmplitude * Math.sin(Math.toRadians(System.currentTimeMillis()) / 2F);

        float useX = (float) (changeFactor * Math.cos(theta));
        float useY = (float) (changeFactor * Math.sin(theta));

        useX = direction * MathUtils.clamp(useX, -halfViewportWidth, halfViewportWidth);
        useY = direction * MathUtils.clamp(useY, -halfViewportHeight, halfViewportHeight);

        float angle = (float) Math.toDegrees(theta) + 45 + (90 * direction);

        // renders in pixel coords
        batch.begin();
        float textureWidth = Styles.arrowTexture.getWidth() / PIXELS_PER_METER;
        float textureHeight = Styles.arrowTexture.getHeight() / PIXELS_PER_METER;
        batch.draw(Styles.arrowTexture, (cameraX + useX), (cameraY + useY), 0, 0, textureWidth, textureHeight, 1, 1,
                angle, 0, 0, Styles.arrowTexture.getWidth(), Styles.arrowTexture.getHeight(), false, false);
        batch.end();
    }
}

package info.shusako.engine.ai.camera;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Shusako on 10/24/2016.
 * For project Voidwalker, 2016
 */
public interface FollowingCameraKernel {

    /**
     * Updates all the camera properties to be in accordance to new information.
     */
    void update();

    /**
     * Renders the camera to the screen
     *
     * @param batch  The batch the it should be rendered to
     * @param delta  The delta time between the current frame and the last frame
     * @param x      The x coordinate, in screen space, where the camera should be rendered
     * @param y      The y coordinate, in screen space, where the camera should be rendered
     * @param width  The width, measured in screen space, where the camera is allowed to render.
     * @param height The height, measured in screen space, where the camera is allowed to render.
     */
    void render(SpriteBatch batch, float delta, int x, int y, int width, int height);
}

package info.shusako.engine.ai.camera;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.FollowingCameraComponent;
import info.shusako.voidwalker.util.MathUtil;
import info.shusako.voidwalker.world.WorldHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shusako on 9/17/2016.
 * Made for project Voidwalker
 */
public class FollowingCameraAI implements FollowingCamera {

    private Camera camera;
    private Entity focusedEntity;
    private Entity defaultEntity;
    private List<Entity> ignoreList = new ArrayList<>();

    private float aspectRatio;
    /**
     * The rate at which the cameras position and viewport dimensions will be interpolated. Default 0.025.
     */
    private float interpolationRate = 0.0275F;

    /**
     * The maximum distance two entities can be to be shown in the same frame. Default 50.
     */
    private int maxDistance = 50;
    private float minViewportWidth;
    private float minViewportHeight;

    /**
     * Multiply the initial viewport estimates by this value in order to increase or decrease the size of the viewport.
     * Default 1.5.
     */
    private float viewportDimensionMultiplier = 1.5F;

    public FollowingCameraAI(Camera primaryCamera) {
        this.camera = primaryCamera;

        setMinimumDimensions(primaryCamera.viewportWidth, primaryCamera.viewportHeight);
    }

    @Override
    public void setMinimumDimensions(float width, float height) {
        this.minViewportWidth = width;
        this.minViewportHeight = height;
        this.aspectRatio = minViewportWidth / minViewportHeight;
    }

    @Override
    public void setMaximumDistance(int maximumDistance) {
        this.maxDistance = maximumDistance;
    }

    @Override
    public void setInterpolationRate(float interpolationRate) {
        this.interpolationRate = interpolationRate;
    }

    @Override
    public Camera getCamera() {
        return this.camera;
    }

    @Override
    public Entity getFocusedEntity() {
        return this.focusedEntity;
    }

    @Override
    public Entity getDefaultEntity() {
        return this.defaultEntity;
    }

    @Override
    public List<Entity> getIgnoreEntityList() {
        return this.ignoreList;
    }

    @Override
    public boolean isRegular(FollowingCameraComponent followingCameraComponent) {
        if((followingCameraComponent.oneTime && followingCameraComponent.oneTimeDisplayTime > 0) ||
                followingCameraComponent.canStealFocus) {
            return false; // it is super priority entity
        }

        return true;
    }

    @Override
    public void getValidEntities(Array<Entity> regularEntities, Array<Entity> superPriorityEntities) {
//        for(Entity entity : WorldHandler.getHandler().getAshleyHandler().getEngine().getEntities()) {
        // If they have a follower component and a box2d component (needed for position)
//            if(FollowingCameraComponent.mapper.get(entity) != null && Box2DComponent.mapper.get(entity) != null &&
//                    !ignoreList.contains(entity)) {

        for(Entity entity : WorldHandler.getHandler().getAshleyHandler().getEngine().getEntitiesFor(Family.all
                (FollowingCameraComponent.class).get())) {
            // If you're not ignoring the entity, then process it
            if(!ignoreList.contains(entity)) {
                FollowingCameraComponent followingCameraComponent = FollowingCameraComponent.mapper.get(entity);
                if(isRegular(followingCameraComponent)) {
                    regularEntities.add(entity);
                } else {
                    superPriorityEntities.add(entity);
                }
            }
        }
    }

    @Override
    public Entity getPriorityEntity(Array<Entity> regularEntities) {
        int highestPriority = Integer.MIN_VALUE;
        Entity priorityEntity = null;

        // Get the regular priority entity, aka the non one-timers
        for(Entity entity : regularEntities) {
            FollowingCameraComponent followingCameraComponent = FollowingCameraComponent.mapper.get(entity);
            if(!followingCameraComponent.oneTime && followingCameraComponent.priority > highestPriority) {
                highestPriority = followingCameraComponent.priority;
                priorityEntity = entity;
            }
        }

        return priorityEntity;
    }

    @Override
    public Entity getSuperPriorityEntity(Array<Entity> superPriorityEntities, Entity priorityEntity) {
        for(Entity entity : superPriorityEntities) {
            FollowingCameraComponent followingCameraComponent = FollowingCameraComponent.mapper.get(entity);
            if(followingCameraComponent.oneTime && followingCameraComponent.oneTimeDisplayTime > 0 && MathUtil
                    .getDistance(entity, priorityEntity) < maxDistance) {
                followingCameraComponent.oneTimeDisplayTime -= Gdx.graphics.getDeltaTime();
                return entity;
            }
            if(followingCameraComponent.canStealFocus && MathUtil.getDistance(entity, priorityEntity) <
                    followingCameraComponent.stealFocusDistance) {
                return entity;
            }
        }
        return null;
    }

    @Override
    public Vector3 getTargetCameraPosition(Array<Entity> validEntities, Vector2 priorityPosition, int maxDistance) {
        Vector3 cameraPosition = new Vector3(0, 0, 0);
        int entityCount = 0;

        for(Entity entity : validEntities) {
            Box2DComponent box2DComponent = Box2DComponent.mapper.get(entity);

            if(MathUtil.getDistance(box2DComponent.body.getPosition(), priorityPosition) < maxDistance) {
                cameraPosition.x += box2DComponent.body.getPosition().x;
                cameraPosition.y += box2DComponent.body.getPosition().y;
                entityCount++;
            }
        }

        cameraPosition.x /= entityCount;
        cameraPosition.y /= entityCount;

        return cameraPosition;
    }

    @Override
    public Vector2 getTargetViewportDimensions(Array<Entity> validEntities, Vector2 priorityPosition, int
            maxDistance, float minWidth, float minHeight) {
        Vector2 minimumDimensions = new Vector2(0, 0);

        /*
         * Determine minimum dimensions just to keep all entities in view
         */
        for(Entity entity : validEntities) {
            Box2DComponent box2DComponent = Box2DComponent.mapper.get(entity);

            if(MathUtil.getDistance(box2DComponent.body.getPosition(), priorityPosition) < maxDistance) {
                if(Math.abs(box2DComponent.body.getPosition().x - priorityPosition.x) > minimumDimensions.x / 2F) {
                    minimumDimensions.x = Math.abs(box2DComponent.body.getPosition().x - priorityPosition.x);
                }
                if(Math.abs(box2DComponent.body.getPosition().y - priorityPosition.y) > minimumDimensions.y / 2F) {
                    minimumDimensions.y = Math.abs(box2DComponent.body.getPosition().y - priorityPosition.y);
                }
            }
        }

        /*
         * Expand minimum dimensions by 50%
         */
        minimumDimensions.x *= viewportDimensionMultiplier;
        minimumDimensions.y *= viewportDimensionMultiplier;

        /*
         * Enforce aspect ratio.
         */
        Vector2 validViewportDimensions = new Vector2(0, 0);
        if(minimumDimensions.y * aspectRatio > minimumDimensions.x) {
            validViewportDimensions.y = minimumDimensions.y;
            validViewportDimensions.x = minimumDimensions.y * aspectRatio;
        } else {
            validViewportDimensions.x = minimumDimensions.x;
            validViewportDimensions.y = minimumDimensions.x / aspectRatio;
        }

        /*
         * Enforce minimum dimensions
         */
        if(validViewportDimensions.x < minWidth || validViewportDimensions.y < minHeight) {
            validViewportDimensions.x = minWidth;
            validViewportDimensions.y = minHeight;
        }

        return validViewportDimensions;
    }

    @Override
    public void interpolateCameraProperties(Camera camera, Array<Entity> validEntities, Vector2 priorityPosition) {
        /*
         * Get valid viewport dimensions that the camera should smoothly interpolate to.
         */
        Vector2 targetViewportDimensions = getTargetViewportDimensions(validEntities, priorityPosition, maxDistance,
                minViewportWidth, minViewportHeight);
        Vector2 viewportDimensions = new Vector2(camera.viewportWidth, camera.viewportHeight);
//        viewportDimensions.set(targetViewportDimensions);
        viewportDimensions.interpolate(targetViewportDimensions, interpolationRate, Interpolation.exp5Out);
        camera.viewportWidth = viewportDimensions.x;
        camera.viewportHeight = viewportDimensions.y;

        /*
         * Finds the final cameraPosition that the camera will focus on.
         */
        Vector3 targetCameraPosition = getTargetCameraPosition(validEntities, priorityPosition, maxDistance);

//        camera.position.set(targetCameraPosition);
        camera.position.interpolate(targetCameraPosition, interpolationRate, Interpolation.exp5Out);

        camera.update();
    }

    @Override
    public void update() {
        /*
         * Gets a list of regular entities, and one time entities
         * Regular entities are entities that have both a FollowingCameraComponent and a Box2DComponent attached
         * where entity->followingCameraComponent->oneTime == false
         */
        Array<Entity> regularEntities = new Array<>();
        Array<Entity> superPriorityEntities = new Array<>();
        getValidEntities(regularEntities, superPriorityEntities);

        /*
         * If there aren't any regular entities to sort through, then we need to stop.
         * We can't find superPriorityEntities without having an anchor for position, otherwise we risk exposing one
         * time entities before they are discovered.
         * NullPointerException is caused if there aren't any regular entities that can be registered by this follower.
         */
        if(regularEntities.size == 0) {
            return;
        }

        /*
         * Finds the priority entity, which is the entity whose position is one that the camera is most concerned with
         * containing at a reasonable zoom.
         */
        this.defaultEntity = getPriorityEntity(regularEntities);
        Entity oneTimeEntity = getSuperPriorityEntity(superPriorityEntities, this.defaultEntity);
        this.focusedEntity = oneTimeEntity != null ? oneTimeEntity : this.defaultEntity;

        /*
         * Come up with the concerned valid entities for camera position and dimension scaling.
         * If the oneTimeEntity is null, aka there isn't a nearby entity that demands focus, then we are only
         * concerned with display the regular entities.
         * Otherwise we only want to focus on the demanding entity, where it is the only valid entity.
         */
        Array<Entity> validEntities;
        if(oneTimeEntity == null) {
            validEntities = regularEntities;
        } else {
            validEntities = new Array<>();
            validEntities.add(this.focusedEntity);
        }

        /*
         * Interpolate both cameras position and dimensions to fit
         */
        this.interpolateCameraProperties(camera, validEntities, Box2DComponent.mapper.get(focusedEntity).body
                .getPosition());
    }

    @Override
    public void render(SpriteBatch batch, float delta, int x, int y, int width, int height) {
        Gdx.gl.glViewport(x, y, width, height);
        WorldHandler.getHandler().render((OrthographicCamera) camera, batch);
    }
}

package info.shusako.engine.ai.camera;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import info.shusako.voidwalker.entities.components.FollowingCameraComponent;

import java.util.List;

/**
 * Created by Shusako on 10/27/2016.
 * For project Voidwalker, 2016
 */
public interface FollowingCamera extends FollowingCameraKernel {

    /**
     * Gets the camera that the following camera AI is following.
     *
     * @return The camera we are following.
     */
    Camera getCamera();

    /**
     * Determines the center position for the camera
     *
     * @param validEntities    A list of valid entities to look through
     * @param priorityPosition The priority position for the camera to focus on
     * @param maxDistance      The maximum distance an entity is allowed to be from the priority position before it's
     *                         ignored.
     *
     * @return The desired center position of the camera
     */
    Vector3 getTargetCameraPosition(Array<Entity> validEntities, Vector2 priorityPosition, int maxDistance);

    /**
     * Gets valid viewport dimensions where all important entities within a given distance of a prioritized position
     * will be seen. The aspect ratio of the minimum dimensions will be enforced.
     *
     * @param validEntities    A list of valid entities to look through
     * @param priorityPosition The position the camera should be most focused with seeing
     * @param maxDistance      The maximum distance an entity is allowed to be from the priority position until it is
     *                         ignored
     * @param minWidth         The minimum width that should be enforced
     * @param minHeight        The minimum height that should be enforced
     *
     * @return A valid viewport dimension where every entity within maxDistance of the priorityPosition will be seen as
     * a reasonable zoom.
     */
    Vector2 getTargetViewportDimensions(Array<Entity> validEntities, Vector2 priorityPosition, int maxDistance, float
            minWidth, float minHeight);

    /**
     * Sets the minimum allowed dimensions for the camera
     *
     * @param width  Minimum allowed width, the cameras viewport width will not be smaller than this.
     * @param height Minimum allowed height, the cameras viewport height will not be smaller than this.
     */
    void setMinimumDimensions(float width, float height);

    /**
     * Sets the maximum distance two entities can be to be shown in the same frame.
     *
     * @param maximumDistance The preferred maximum distance.
     */
    void setMaximumDistance(int maximumDistance);

    /**
     * Sets the rate at which the cameras position and viewport dimensions will be interpolated.
     *
     * @param interpolationRate The preferred interpolation rate.
     */
    void setInterpolationRate(float interpolationRate);

    /**
     * @return The current entity in focus
     */
    Entity getFocusedEntity();

    /**
     * This is used in place of {@code::getFocusedEntity} when you want the entity of regular priority to be returned,
     * instead of the entity that the camera is immediately focusing on (which could be of super priority).
     *
     * @return The default entity that the camera will focus on.
     */
    Entity getDefaultEntity();

    /**
     * Returns the entity ignore list.
     */
    List<Entity> getIgnoreEntityList();

    /**
     * Determines if the provided following component should be classified as a regular priority, to be sorted into the
     * regular priority list, or as super priority, to be sorted into the super priority list.
     *
     * @param followingCameraComponent The component in question.
     *
     * @return true iff the component should be in the regular priority list, false if it should be in the super
     * priority list
     */
    boolean isRegular(FollowingCameraComponent followingCameraComponent);

    /**
     * @param regularEntities       A non-null Array of type Entity.
     * @param superPriorityEntities A non-null Array of type Entity.
     *
     * @requires {@code WorldHandler.getHandler().getAshleyHandler().getEngine().getEntities()} is a valid path.
     * @Postcondition {@code regularEntities} will be a @NotNull list of valid entities, where a valid entity is
     * defined as an entity with a FollowingCameraComponent, where followingCameraComponent.oneTime is false, and a
     * Box2DComponent attached. It will only contain current entities.
     * @Postcondition {@code superPriorityEntities} will be a @NotNull list of entities with a
     * FollowingCameraComponent, where followingCameraComponent.oneTime is true, and a Box2DComponent attached. It will
     * only contain current entities.
     */
    void getValidEntities(Array<Entity> regularEntities, Array<Entity> superPriorityEntities);

    /**
     * Checks all entities for being an entity that the Camera wants to focus on, and returns the most important entity.
     *
     * @param regularEntities A list of regular entities
     *
     * @return The priority entity
     */
    Entity getPriorityEntity(Array<Entity> regularEntities);


    /**
     * Returns the entity that is best fit for stealing focus from the default priority entity that is passed.
     *
     * @param superPriorityEntities The list of entities that have a super priority flag in their camera follower AI.
     *
     * @return The super priority entity that has a best fit for stealing focus from the priorityEntity.
     */
    Entity getSuperPriorityEntity(Array<Entity> superPriorityEntities, Entity priorityEntity);

    /**
     * Interpolates the cameras position and dimensions to fit all needed entities onto the screen
     *
     * @param camera           The screen whose properties will be updated
     * @param validEntities    A list of valid entities to iterate over
     * @param priorityPosition The position to be given viewing weight
     */
    void interpolateCameraProperties(Camera camera, Array<Entity> validEntities, Vector2 priorityPosition);
}

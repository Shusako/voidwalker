package info.shusako.engine.loader.layer;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.collision.CollisionCategory;
import info.shusako.engine.loader.LayerLoader;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public class ObjectLayerLoader implements LayerLoader {

    public static final String layerType = "objectLayer";

    @Override
    public String layerType() {
        return layerType;
    }

    @Override
    public void load(MapLayer layer) {
        MapObjects mapObjects = layer.getObjects();

        for(MapObject mapObject : mapObjects) {
            float x = (float) mapObject.getProperties().get("x");
            float y = (float) mapObject.getProperties().get("y");
            float w = (float) mapObject.getProperties().get("width");
            float h = (float) mapObject.getProperties().get("height");
            Rectangle rectangle = new Rectangle(x, y, w, h);

            Body body = Box2DUtility.createBody((rectangle.getX() + rectangle.getWidth() / 2) / PIXELS_PER_METER,
                    (rectangle.getY() + rectangle.getHeight() / 2) / PIXELS_PER_METER, BodyDef.BodyType.StaticBody);
            Box2DUtility.createCollisionBox(body, "world", (rectangle.getWidth() / 2) / PIXELS_PER_METER, (rectangle
                    .getHeight() / 2) / PIXELS_PER_METER, 1.0F, 0.2F, 0.0F, CollisionCategory.WORLD,
                    CollisionCategory.EVERYTHING);
        }
    }
}

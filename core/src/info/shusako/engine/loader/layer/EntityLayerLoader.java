package info.shusako.engine.loader.layer;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import info.shusako.engine.loader.LayerLoader;
import info.shusako.engine.entities.EntityBase;
import info.shusako.engine.entities.EntityMap;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public class EntityLayerLoader implements LayerLoader {

    public static final String layerType = "entityLayer";

    @Override
    public String layerType() {
        return layerType;
    }

    @Override
    public void load(MapLayer layer) {
        MapObjects mapObjects = layer.getObjects();

        for(MapObject mapObject : mapObjects) {
            int entityID = Integer.parseInt((String) mapObject.getProperties().get("entityID"));
            EntityBase entityBase = EntityMap.getEntity(entityID);

            float x = (float) mapObject.getProperties().get("x") / PIXELS_PER_METER;
            float y = (float) mapObject.getProperties().get("y") / PIXELS_PER_METER;
            entityBase.createEntity(x, y);

            System.out.println("entityID = " + entityID);
            System.out.println("x = " + x);
            System.out.println("y = " + y);
        }
    }
}

package info.shusako.engine.loader;

import com.badlogic.gdx.maps.MapLayer;

/**
 * Created by Shusako on 12/18/2016.
 * For project voidwalker, 2016
 */
public interface LayerLoader {

    /**
     * The layer type identifier string that this loader can handle properly.
     *
     * @return The layer identifier.
     */
    String layerType();

    /**
     * Loads the provided mapLayer into the existing world.
     *
     * @param layer The layer that needs to be loaded.
     */
    void load(MapLayer layer);
}

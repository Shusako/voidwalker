package info.shusako.engine.loader;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import info.shusako.engine.loader.layer.EntityLayerLoader;
import info.shusako.engine.loader.layer.ObjectLayerLoader;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Shusako on 11/6/2016.
 * For project Voidwalker, 2016
 */
public class MapLoaderTMX {

    /**
     * Private constructor to prevent instantiation of this utility class.
     */
    private MapLoaderTMX() {

    }

    private static Map<String, LayerLoader> layerLoaders;

    static {
        layerLoaders = new HashMap<>();

        register(new EntityLayerLoader());
        register(new ObjectLayerLoader());
    }

    /**
     * Registers the layer loader into the layerLoaders map
     *
     * @param layerLoader The layer loader to be registered.
     */
    private static void register(LayerLoader layerLoader) {
        layerLoaders.put(layerLoader.layerType(), layerLoader);
    }

    /**
     * Loads the inputted map name into memory, ready to be played.
     *
     * @param mapName The map name that correlates to the map on file.
     *
     * @Precondition The map exists on file.
     * All tmx object layers have a defined type property that exists in the layerLoaders map.
     */
    public static TiledMap loadMap(String mapName) {
        File f = new File(".");
        System.out.println(Arrays.toString(f.listFiles()));

        TiledMap map = new TmxMapLoader().load("map/" + mapName + ".tmx");

        Iterator<String> iterator = map.getProperties().getKeys();
        while(iterator.hasNext()) {
            String value = iterator.next();
            System.out.println(value);
        }

        for(MapLayer layer : map.getLayers()) {

            if(layer.getProperties().containsKey("type")) {
                String type = (String) layer.getProperties().get("type");

                LayerLoader layerLoader = layerLoaders.get(type);
                if(layerLoader != null) {
                    layerLoader.load(layer);
                }
            }
        }

        return map;
    }
}

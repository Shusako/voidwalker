package info.shusako.engine.entities;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import info.shusako.voidwalker.entities.components.AnimationComponent;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.TextureComponent;
import info.shusako.voidwalker.world.WorldHandler;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 10/21/2016.
 * For project Voidwalker, 2016
 */
public class EntityRenderer {

    private WorldHandler worldHandler;

    public EntityRenderer(WorldHandler worldHandler) {
        this.worldHandler = worldHandler;
    }

    /**
     * Renders all the entities in {var:this.worldHandler}
     *
     * @param batch The SpriteBatch for the entities to be rendered to
     */
    public void render(Batch batch) {
        for(Entity entity : this.worldHandler.getAshleyHandler().getEngine().getEntities()) {
            renderTextureComponent(batch, entity);
            renderAnimationComponent(batch, entity);
        }
    }

    /**
     * Renders the entities animation component to the batch
     *
     * @param batch  The SpriteBatch for the entity to be rendered to
     * @param entity The entity to be rendered
     */
    private void renderAnimationComponent(Batch batch, Entity entity) {
        AnimationComponent animationComponent = AnimationComponent.mapper.get(entity);
        if(animationComponent != null) {
            Box2DComponent box2DComponent = Box2DComponent.mapper.get(entity);
            Animation animation = animationComponent.animations.get(animationComponent.currentAnimation);
            TextureRegion keyFrame = animation.getKeyFrame(animationComponent.timePassed, true);
            // this is only the widths being scaled, not the positions
            float width = keyFrame.getRegionWidth() / PIXELS_PER_METER;
            float height = keyFrame.getRegionHeight() / PIXELS_PER_METER;
            batch.draw(keyFrame, box2DComponent.body.getPosition().x - (width / 2), box2DComponent.body.getPosition()
                    .y - (height / 2), width, height);
        }
    }

    /**
     * Renders the entities texture component to the batch
     *
     * @param batch  The SpriteBatch for the entity to be rendered to
     * @param entity The entity to be rendered
     */
    private void renderTextureComponent(Batch batch, Entity entity) {
        TextureComponent textureComponent = TextureComponent.mapper.get(entity);
        if(textureComponent != null) {
            // If they have a texture component, they'll have a box2d component, at least for now.
            Box2DComponent box2DComponent = Box2DComponent.mapper.get(entity);
            TextureRegion textureRegion = textureComponent.textureRegion;
            float width = textureRegion.getRegionWidth() / PIXELS_PER_METER;
            float height = textureRegion.getRegionHeight() / PIXELS_PER_METER;
            batch.draw(textureRegion, box2DComponent.body.getPosition().x - (width / 2), box2DComponent.body
                    .getPosition().y - (width / 2), width, height);
        }
    }

    public void update(float delta) {
        // Update animation step
        for(Entity entity : this.worldHandler.getAshleyHandler().getEngine().getEntities()) {
            AnimationComponent animationComponent = AnimationComponent.mapper.get(entity);
            if(animationComponent != null) {
                animationComponent.timePassed += delta;
            }
        }
    }
}

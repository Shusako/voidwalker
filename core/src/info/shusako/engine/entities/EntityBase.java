package info.shusako.engine.entities;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import info.shusako.voidwalker.world.WorldHandler;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public abstract class EntityBase {

    /**
     * Should be useless, prevent double instantiation of entities. Must use the EntityMap class to access.
     */
    private boolean hasBeenCreated = false;

    /**
     * Protected so it can only be instantiated from the current package, specifically the EntityMap class.
     */
    protected EntityBase() {
        if(hasBeenCreated) {
            System.err.println("Re-instantiation of entity is not allowed!");
        }

        hasBeenCreated = true;
    }

    /**
     * Creates an entity of a certain subtype and adds all the necessary components to it.
     * Adds the entity to the engine.
     *
     * @param pass An object list that should be passed to the entity upon component creation // TODO: is this good?
     */
    public final void createEntity(Object... pass) {
        Entity entity = new Entity();

        Component[] components = constructComponents(entity, pass);
        for(Component component : components) {
            entity.add(component);
        }

        WorldHandler.getHandler().getAshleyHandler().getEngine().addEntity(entity);
    }

    public abstract Component[] constructComponents(Entity entity, Object... pass);

    /**
     * @return An identifier to the entity {@code this}.
     */
    public abstract String getIdentifier();
}

package info.shusako.engine.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shusako on 10/21/2016.
 * For project Voidwalker, 2016
 */
public class EntityMap {

    private static Map<Integer, EntityBase> idToClassMap = new HashMap<>();

    private static int counter;

    public static void registerEntity(EntityBase entity) {
        idToClassMap.put(++counter, entity);
    }

    public static EntityBase getEntity(int entityID) {
        return idToClassMap.get(entityID);
    }

    public static EntityBase getEntity(Class clazz) {
        for(EntityBase entityBase : idToClassMap.values()) {
            if(entityBase.getClass().equals(clazz)) {
                return entityBase;
            }
        }
        return null;
    }
}

package info.shusako.voidwalker;

import com.badlogic.gdx.Game;
import info.shusako.engine.entities.EntityMap;
import info.shusako.engine.states.States;
import info.shusako.voidwalker.display.menus.InGame;
import info.shusako.voidwalker.display.util.Styles;
import info.shusako.voidwalker.entities.entities.model.Player;
import info.shusako.voidwalker.entities.entities.model.RandomNPC;
import info.shusako.voidwalker.entities.entities.model.Rock;
import info.shusako.voidwalker.world.WorldHandler;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class Voidwalker extends Game {

    private static Voidwalker instance;

    public static Voidwalker getInstance() {
        return instance;
    }

    @Override
    public void create() {
        States.PIXELS_PER_METER = 32F;

        EntityMap.registerEntity(new Player());
        EntityMap.registerEntity(new RandomNPC());
        EntityMap.registerEntity(new Rock());

        instance = this;

        String map;
        if(isDevelopmentEnvironment()) {
            map = "TestMap";
        } else {
            map = JOptionPane.showInputDialog(null, "What map would you like to load?");
        }
        WorldHandler.getHandler().setActiveMap(map);

        this.setScreen(new InGame());
    }

    private boolean isDevelopmentEnvironment() {
        try {
            return new File(".").getCanonicalPath().toLowerCase().contains("voidwalker");
        } catch(IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void dispose() {
        super.dispose();
        Styles.dispose();
        WorldHandler.getHandler().dispose();
    }
}

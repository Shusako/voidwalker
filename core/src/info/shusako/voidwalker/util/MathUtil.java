package info.shusako.voidwalker.util;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import info.shusako.voidwalker.entities.components.Box2DComponent;

/**
 * Created by Shusako on 10/24/2016.
 * For project Voidwalker, 2016
 */
public class MathUtil {

    /**
     * Gets the distance between two Vector2 Positions
     *
     * @param pos1 The position of the first point
     * @param pos2 The position of the second point
     *
     * @return The distance between the two positions
     */
    public static float getDistance(Vector2 pos1, Vector2 pos2) {
        Vector2 distance = pos1.cpy().sub(pos2);
        return distance.len();
    }

    public static float getDistance(Entity firstEntity, Entity secondEntity) {
        return getDistance(Box2DComponent.mapper.get(firstEntity).body.getPosition(), Box2DComponent.mapper.get
                (secondEntity).body.getPosition());
    }
}

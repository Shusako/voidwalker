package info.shusako.voidwalker.util;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;

import static info.shusako.engine.states.States.PIXELS_PER_METER;
import static info.shusako.voidwalker.display.menus.InGame.SCREEN_WIDTH_IN_METERS_BASE;

/**
 * Created by Shusako on 10/12/2016.
 * For project Voidwalker, 2016
 */
public class UIUtil {

    /**
     * Converts world space to screen render space
     *
     * @param camera The camera that controls the world rendering
     * @param x      The world space x coordinate
     * @param y      The world space y coordinate
     *
     * @return A Vector2 of renderCoordinates
     */
    public static Vector2 worldSpaceToScreenSpace(Camera camera, float x, float y) {
        float halfWidth = (camera.viewportWidth / 2);
        float halfHeight = (camera.viewportHeight / 2);
        float relX = (camera.position.x - x);
        float relY = (camera.position.y - y);
        float scaledPixelsPerMeter = (2 * PIXELS_PER_METER * SCREEN_WIDTH_IN_METERS_BASE) / camera.viewportWidth;
        int renderX = (int) ((halfWidth - relX) * scaledPixelsPerMeter);
        int renderY = (int) ((halfHeight - relY) * scaledPixelsPerMeter);

        return new Vector2(renderX, renderY);
    }

    /**
     * Converts world space to screen render space
     *
     * @param camera The camera that controls the world rendering
     * @param pos    The world space position
     *
     * @return A Vector2 of renderCoordinates
     */
    public static Vector2 worldSpaceToScreenSpace(Camera camera, Vector2 pos) {
        return worldSpaceToScreenSpace(camera, pos.x, pos.y);
    }

    /**
     * When the camera zooms, the pixels per meter changes. This will return that value.
     *
     * @param camera The camera who we want the PPM for
     *
     * @return the scaled pixels per meter based on the camera viewport
     */
    public static float scaledPixelsPerMeter(Camera camera) {
        return (2 * PIXELS_PER_METER * SCREEN_WIDTH_IN_METERS_BASE) / camera.viewportWidth;
    }
}

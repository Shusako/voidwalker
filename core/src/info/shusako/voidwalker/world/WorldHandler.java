package info.shusako.voidwalker.world;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import info.shusako.engine.collision.CollisionListener;
import info.shusako.engine.display.RenderableManager;
import info.shusako.engine.display.util.CustomOrthogonalTiledMapRenderer;
import info.shusako.engine.entities.EntityRenderer;
import info.shusako.engine.loader.MapLoaderTMX;
import info.shusako.voidwalker.entities.AshleyHandler;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public class WorldHandler {

    public static final float TIME_STEP = 1 / 60F;
    private static final int VELOCITY_ITER = 6;
    private static final int POSITION_ITER = 2;
    private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);

    /**
     * A renderer that will render Tiled maps.
     */
    private CustomOrthogonalTiledMapRenderer mapRenderer;

    /**
     * AshleyHandler controls the engine and updating entities.
     */
    private AshleyHandler ashleyHandler = new AshleyHandler();
    /**
     * Box2D world instance, holds bodies.
     */
    private World world = new World(new Vector2(0, -22), true);

    /**
     * The worlds contact listener. Receives information about collisions starting and ending and redistributes the
     * information to callbacks that concern it.
     */
    private final CollisionListener collisionListener;

    /**
     * Single class instance for access anywhere.
     */
    private static WorldHandler instance = new WorldHandler();

    /**
     * No public instantiation is allowed.
     */
    private WorldHandler() {
        Box2D.init();
        entityRenderer = new EntityRenderer(this);

        this.collisionListener = new CollisionListener();
        getWorld().setContactListener(this.collisionListener);

        mapRenderer = new CustomOrthogonalTiledMapRenderer(null, 1F / PIXELS_PER_METER);
    }

    public void setActiveMap(String mapName) {
        TiledMap map = MapLoaderTMX.loadMap(mapName);
        mapRenderer.setMap(map);
    }

    private EntityRenderer entityRenderer;

    /**
     * @return The singleton instance of WorldHandler
     */
    public static WorldHandler getHandler() {
        return instance;
    }

    public void render(OrthographicCamera camera, SpriteBatch batch) {
        batch.begin();

        // batch.setProjectionMatrix(camera.combined);
        // this is implicit with the next call
        mapRenderer.render(batch, camera, entityRenderer);
        RenderableManager.getInstance().render(batch, camera);

        debugRenderer.render(world, camera.combined);
        batch.end();
    }

    public void update(float delta) {
        getAshleyHandler().update(delta);
        doPhysicsStep(delta);
        entityRenderer.update(delta);
    }

    private float accumulator = 0;

    private void doPhysicsStep(float deltaTime) {
        // fixed time step
        // max frame time to avoid spiral of death (on slow devices)
        float frameTime = Math.min(deltaTime, 0.25F);
        accumulator += frameTime;
        while(accumulator >= TIME_STEP) {
            world.step(TIME_STEP, VELOCITY_ITER, POSITION_ITER);
            accumulator -= TIME_STEP;
        }
    }

    public void dispose() {
        debugRenderer.dispose();
        mapRenderer.dispose();
        world.dispose();
    }

    /*
     * Getters & Setters
     */

    /**
     * @return The active collision callback listener
     */
    public CollisionListener getCollisionListener() {
        return collisionListener;
    }

    /**
     * @return The ashley handler instance
     */
    public AshleyHandler getAshleyHandler() {
        return ashleyHandler;
    }

    public World getWorld() {
        return world;
    }
}

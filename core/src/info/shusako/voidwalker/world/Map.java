package info.shusako.voidwalker.world;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import info.shusako.voidwalker.entities.AshleyHandler;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 10/3/2016.
 * For project Voidwalker, 2016
 */
public class Map {

    private AshleyHandler ashleyHandler;
    private Texture texture;

    private World world;

    public void set(World world, Texture texture) {
        this.world = world;
        this.ashleyHandler = new AshleyHandler();
        this.texture = texture;
    }

    public World getWorld() {
        return this.world;
    }

    public AshleyHandler getAshleyHandler() {
        return ashleyHandler;
    }

    public void render(SpriteBatch batch) {
        batch.draw(texture, 0, 0, texture.getWidth() / PIXELS_PER_METER, texture.getHeight() / PIXELS_PER_METER);
    }
}

package info.shusako.voidwalker.world.collision;

/**
 * Created by Shusako on 1/21/2017.
 * For project Voidwalker, 2017
 */
public final class CollisionCategory extends info.shusako.engine.collision.CollisionCategory {

    public static final short PLAYER = 1 << 1;
}

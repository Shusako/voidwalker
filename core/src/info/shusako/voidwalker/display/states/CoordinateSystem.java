package info.shusako.voidwalker.display.states;

import info.shusako.engine.display.states.CoordinateStateSwitcher;
import info.shusako.voidwalker.display.states.switchers.PixelCoordinateStateSwitcher;
import info.shusako.voidwalker.display.states.switchers.ScreenCoordinateStateSwitcher;
import info.shusako.voidwalker.display.states.switchers.WorldCoordinateStateSwitcher;

/**
 * Created by Shusako on 1/28/2017.
 * For project Voidwalker, 2017
 */
public enum CoordinateSystem implements info.shusako.engine.display.states.CoordinateSystem {

    WORLD(new WorldCoordinateStateSwitcher()), PIXEL(new PixelCoordinateStateSwitcher()), SCREEN(new
            ScreenCoordinateStateSwitcher());


    private CoordinateStateSwitcher coordinateStateSwitcher;

    @Override
    public CoordinateStateSwitcher getCoordinateStateSwitcher() {
        return this.coordinateStateSwitcher;
    }

    CoordinateSystem(CoordinateStateSwitcher coordinateStateSwitcher) {
        this.coordinateStateSwitcher = coordinateStateSwitcher;
    }
}

package info.shusako.voidwalker.display.states.switchers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import info.shusako.engine.display.states.CoordinateStateSwitcher;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public class WorldCoordinateStateSwitcher implements CoordinateStateSwitcher {

    @Override
    public void enable(Batch batch, Camera camera) {
        batch.setProjectionMatrix(camera.combined);
        // don't need to do anything, default is world state
    }

    @Override
    public void disable(Batch batch, Camera camera) {
        batch.setProjectionMatrix(camera.combined);
        // don't need to do undo anything, default is world state
    }
}

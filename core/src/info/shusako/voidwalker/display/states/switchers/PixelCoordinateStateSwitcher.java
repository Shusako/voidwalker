package info.shusako.voidwalker.display.states.switchers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import info.shusako.engine.display.states.CoordinateStateSwitcher;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public class PixelCoordinateStateSwitcher implements CoordinateStateSwitcher {

    @Override
    public void enable(Batch batch, Camera camera) {
        camera.combined.scl(1 / PIXELS_PER_METER);
        batch.setProjectionMatrix(camera.combined);
    }

    @Override
    public void disable(Batch batch, Camera camera) {
        camera.combined.scl(PIXELS_PER_METER);
        batch.setProjectionMatrix(camera.combined);
    }
}

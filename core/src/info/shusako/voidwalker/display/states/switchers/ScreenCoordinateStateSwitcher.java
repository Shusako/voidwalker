package info.shusako.voidwalker.display.states.switchers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import info.shusako.engine.display.states.CoordinateStateSwitcher;

/**
 * Created by Shusako on 12/12/2016.
 * For project Voidwalker, 2016
 */
public class ScreenCoordinateStateSwitcher implements CoordinateStateSwitcher {

    @Override
    public void enable(Batch batch, Camera camera) {
        System.err.println("Screen Coordinate State Switcher not yet implemented!");
        // TODO: IMPLEMENT
    }

    @Override
    public void disable(Batch batch, Camera camera) {
        // TODO: IMPLEMENT
    }
}

package info.shusako.voidwalker.display.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Shusako on 8/16/2016.
 * For project Voidwalker, 2016
 */
public class Styles {

    //    public static final BitmapFont font = new BitmapFont(Gdx.files.internal("font/testfont.fnt"));
    public static final BitmapFont font = new BitmapFont();

    static {
        font.setColor(1, 1, 1, 1);
        font.setUseIntegerPositions(false);
        font.getData().setScale(0.5F);
    }

    private static final TextureAtlas buttonAtlas = new TextureAtlas("button.atlas");
    private static final Skin buttonSkin = new Skin(buttonAtlas);
    private static final TextButton.TextButtonStyle style = new TextButton.TextButtonStyle(Styles.buttonSkin
            .getDrawable("buttonNormal"), null, null, Styles.font);

    public static void addButton(String name, int x, int y, int width, int height, final Runnable runnable, Stage
            stage) {
        TextButton button = new TextButton(name, style);
        button.setBounds(x, y, width, height);
        button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                runnable.run();
                return super.touchDown(event, x, y, pointer, button);
            }
        });

        stage.addActor(button);
    }

    public static void drawString(Batch batch, String text, float x, float y) {
        font.draw(batch, text, x, y);
    }

    public static Texture arrowTexture = new Texture("res/pointer.png");
    public static Texture separatorTexture = new Texture("res/separator.png");

    public static void dispose() {
        System.out.println("Styles DISPOSED");
        buttonSkin.dispose();
        buttonAtlas.dispose();
        font.dispose();

        arrowTexture.dispose();
        separatorTexture.dispose();
    }
}

package info.shusako.voidwalker.display.renderables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.display.Renderable;
import info.shusako.engine.display.RenderableManager;
import info.shusako.voidwalker.display.states.CoordinateSystem;
import info.shusako.voidwalker.display.util.Styles;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.QuestComponent;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 10/28/2016.
 * For project Voidwalker, 2016
 */
public class QuestRenderer implements Renderable {

    private static final Texture questExclamation = new Texture(Gdx.files.internal("res/questExclamation.png"));
    private static final Texture questWindow = new Texture(Gdx.files.internal("res/questWindow.png"));

    private QuestComponent questComponent;
    private Box2DComponent box2DComponent;

    private float offsetX;
    private float offsetY;

    /**
     * @param questComponent The quest component containing the quest information to render.
     * @param box2DComponent The box2DComponent that contains the positional information of the entity whose quest
     *                       should be rendered.
     */
    public QuestRenderer(QuestComponent questComponent, Box2DComponent box2DComponent) {
        this.questComponent = questComponent;
        this.box2DComponent = box2DComponent;

        RenderableManager.getInstance().registerRenderable(this);
        offsetY = Box2DUtility.getHeight(box2DComponent) / 2;
    }

    private GlyphLayout layout = new GlyphLayout();

    @Override
    public void render(SpriteBatch batch, Camera camera) {
        float worldX = (box2DComponent.body.getPosition().x + offsetX);
        float worldY = (box2DComponent.body.getPosition().y + offsetY);

        if(questComponent.inRange) {
            float renderX = worldX * PIXELS_PER_METER - (questWindow.getWidth() / 2);
            float renderY = worldY * PIXELS_PER_METER;
            batch.draw(questWindow, renderX, renderY, questWindow.getWidth(), questWindow.getHeight());
            int padding = 5;
            layout.setText(Styles.font, questComponent.quest.narrative(), Color.BLACK, questWindow.getWidth() -
                    padding * 2, Align.topLeft, true);
            Styles.font.draw(batch, layout, renderX + padding, renderY + questWindow.getHeight() - padding);
        } else {
            batch.draw(questExclamation, worldX * PIXELS_PER_METER - (questExclamation.getWidth() / 2), worldY *
                    PIXELS_PER_METER, questExclamation.getWidth(), questExclamation.getHeight());
        }
    }

    @Override
    public boolean expired() {
        return false;
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        return CoordinateSystem.PIXEL;
    }
}

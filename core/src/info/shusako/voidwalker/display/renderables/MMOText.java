package info.shusako.voidwalker.display.renderables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import info.shusako.engine.display.Renderable;
import info.shusako.engine.display.RenderableManager;
import info.shusako.engine.util.Timer;
import info.shusako.voidwalker.display.states.CoordinateSystem;
import info.shusako.voidwalker.display.util.Styles;
import info.shusako.voidwalker.entities.components.Box2DComponent;

import java.util.Random;

/**
 * Created by Shusako on 10/28/2016.
 * For project Voidwalker, 2016
 */
public class MMOText implements Renderable {

    private static Random random = new Random();
    private Timer expireTimer;
    private Vector2 renderLocation;
    private String text;

    private Vector2 renderOffset;
    private Vector2 renderOffsetVelocity;

    /**
     * Sets up a MMOText to be rendered to the screen at the box2DComponents positions.
     *
     * @param text           The text that should be displayed
     * @param box2DComponent The box component that the text should be displayed at
     * @param durationMS     The time in ms until the text expires
     */
    public MMOText(String text, Box2DComponent box2DComponent, long durationMS) {
        this(text, box2DComponent.body.getPosition().cpy(), durationMS);
        this.renderOffset = new Vector2(0, 0);
        this.renderOffsetVelocity = new Vector2(random.nextFloat() * 50 * (random.nextBoolean() ? 1 : -1), 250 *
                random.nextFloat());
    }

    /**
     * Sets up a MMOText to be rendered to the screen at the given position.
     *
     * @param text       The text that should be displayed
     * @param position   The position that the text should be displayed at
     * @param durationMS The time in ms until the text expires
     */
    public MMOText(String text, Vector2 position, long durationMS) {
        this.expireTimer = new Timer(durationMS);
        this.text = text;
        this.renderLocation = position;

        RenderableManager.getInstance().registerRenderable(this);
    }

    @Override
    public void render(SpriteBatch batch, Camera camera) {
        Styles.drawString(batch, text, renderLocation.x/* * PIXELS_PER_METER*/ + renderOffset.x, renderLocation.y/* *
                PIXELS_PER_METER*/ + renderOffset.y);

        renderOffset.x += this.renderOffsetVelocity.x * Gdx.graphics.getDeltaTime();
        renderOffset.y += this.renderOffsetVelocity.y * Gdx.graphics.getDeltaTime();
        this.renderOffsetVelocity.y -= 5;
    }

    @Override
    public boolean expired() {
        return expireTimer.hasTimePassed();
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        return CoordinateSystem.PIXEL;
    }
}

package info.shusako.voidwalker.display.renderables;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.display.Renderable;
import info.shusako.engine.display.RenderableManager;
import info.shusako.engine.util.Timer;
import info.shusako.voidwalker.display.states.CoordinateSystem;
import info.shusako.voidwalker.display.util.Styles;
import info.shusako.voidwalker.entities.components.Box2DComponent;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 10/12/2016.
 * For project Voidwalker, 2016
 */
public class DialogueText implements Renderable {

    private String text;
    private Vector2 renderLocation;
    private float renderOffset;
    private Timer expireTimer;

    private long startTime;
    private final int msPerCharacter = 65;

    /**
     * @param text           The text that should be displayed
     * @param box2DComponent The box component that the text should be displayed above
     * @param durationMS     The time in ms until the text expires
     */
    public DialogueText(String text, Box2DComponent box2DComponent, long durationMS) {
        this.text = text;
        this.renderLocation = box2DComponent.body.getPosition();
        this.expireTimer = new Timer(durationMS);

        this.renderOffset = Box2DUtility.getHeight(box2DComponent) / 2;

        this.startTime = System.currentTimeMillis();

        RenderableManager.getInstance().registerRenderable(this);
    }

    private GlyphLayout layout = new GlyphLayout();

    @Override
    public void render(SpriteBatch batch, Camera camera) {
        int currentIndex = (int) ((System.currentTimeMillis() - startTime) / msPerCharacter);
        if(currentIndex > this.text.length()) {
            currentIndex = this.text.length();
        }
        String currentText = this.text.substring(0, currentIndex);

        layout.setText(Styles.font, currentText, new Color(1, 1, 1, 1), 150, Align.topLeft, true);

        float worldX = renderLocation.x;
        float worldY = renderLocation.y + renderOffset;

        float renderX = worldX * PIXELS_PER_METER - (layout.width / 2);
        float renderY = worldY * PIXELS_PER_METER + layout.height;

        Styles.font.draw(batch, layout, renderX, renderY);

//        Styles.drawString(batch, text, worldX * PIXELS_PER_METER, worldY * PIXELS_PER_METER);
    }

    @Override
    public boolean expired() {
        return expireTimer.hasTimePassed();
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        return CoordinateSystem.PIXEL;
    }
}

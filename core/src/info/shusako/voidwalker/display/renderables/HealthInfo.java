package info.shusako.voidwalker.display.renderables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.display.Renderable;
import info.shusako.engine.display.RenderableManager;
import info.shusako.voidwalker.display.states.CoordinateSystem;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.HealthComponent;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 10/28/2016.
 * For project Voidwalker, 2016
 */
public class HealthInfo implements Renderable {

    private static final Texture heart = new Texture(Gdx.files.internal("res/heart.png"));
    private static final Texture deadHeart = new Texture(Gdx.files.internal("res/deadheart.png"));

    private HealthComponent healthComponent;
    private Box2DComponent box2DComponent;

    private float offsetY;

    /**
     * @param healthComponent The health component containing the health information to render.
     * @param box2DComponent  The box2DComponent that contains the positional information of the entity whose health
     *                        should be rendered.
     */
    public HealthInfo(HealthComponent healthComponent, Box2DComponent box2DComponent) {
        this.healthComponent = healthComponent;
        this.box2DComponent = box2DComponent;

        RenderableManager.getInstance().registerRenderable(this);
        offsetY = Box2DUtility.getHeight(box2DComponent) / 2;
    }

    @Override
    public void render(SpriteBatch batch, Camera camera) {
        float renderX = (box2DComponent.body.getPosition().x);
        float renderY = (box2DComponent.body.getPosition().y + offsetY);

        float numberOfHearts = (int) ((healthComponent.health / healthComponent.maxHealth) * healthComponent
                .displayHeartCount);

        float width = heart.getWidth() / PIXELS_PER_METER;
        float height = heart.getHeight() / PIXELS_PER_METER;

        renderX -= (numberOfHearts * width) / 2; // offset it to center the health bar

        for(int i = 0; i < healthComponent.displayHeartCount; i++) {
            batch.draw(i < numberOfHearts ? heart : deadHeart, renderX + width * i, renderY, width, height);
        }
    }

    /*
     * Thoughts:
     * Renderable also has an enum specifying which coordinate system it should be rendered in, that way we can deal
      * with the font situation being a cunt and removing the need for all the batch calls.
     */

    @Override
    public boolean expired() {
        return false;
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        return CoordinateSystem.WORLD;
    }
}

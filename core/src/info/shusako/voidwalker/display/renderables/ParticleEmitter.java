package info.shusako.voidwalker.display.renderables;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import info.shusako.engine.display.Renderable;
import info.shusako.engine.display.RenderableManager;
import info.shusako.voidwalker.display.states.CoordinateSystem;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 11/12/2016.
 * For project Voidwalker, 2016
 */
public class ParticleEmitter implements Renderable {

    private ParticleEffect particleEffect;
    private Vector2 position;
    private Vector2 velocity;

    public ParticleEmitter(Vector2 position, Vector2 velocity, String particle) {
        particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal("particles/" + particle), Gdx.files.internal("particles/"));
        particleEffect.getEmitters().first().setPosition(position.x, position.y);
        particleEffect.getEmitters().first().getWind().setActive(true);
        particleEffect.start();

        this.position = position;
        this.velocity = velocity;
        RenderableManager.getInstance().registerRenderable(this);
    }

    @Override
    public void render(SpriteBatch batch, Camera camera) {
        particleEffect.update(Gdx.graphics.getDeltaTime());

        particleEffect.getEmitters().first().setPosition(position.x * PIXELS_PER_METER, position.y * PIXELS_PER_METER);
        // I have no idea why it has to be velX - posX, it seems like velX is the velocity + position
        particleEffect.getEmitters().first().getWind().setHigh((velocity.x - position.x) * PIXELS_PER_METER);

        particleEffect.draw(batch);

        if(particleEffect.isComplete()) {
            particleEffect.reset();
        }
    }

    @Override
    public boolean expired() {
        return false;
    }

    @Override
    public CoordinateSystem getCoordinateSystem() {
        return CoordinateSystem.PIXEL;
    }
}

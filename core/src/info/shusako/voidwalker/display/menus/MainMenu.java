package info.shusako.voidwalker.display.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import info.shusako.voidwalker.Voidwalker;
import info.shusako.voidwalker.display.util.Styles;

/**
 * Created by Shusako on 8/14/2016.
 * For project Voidwalker, 2016
 */
public class MainMenu implements Screen {

    private SpriteBatch batch;
    private Stage stage;
    private Texture titleTexture, backgroundBlurTexture;

    private Image title;

    private OrthographicCamera camera;
    private StretchViewport viewport;

    @Override
    public void show() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport = new StretchViewport(camera.viewportWidth, camera.viewportHeight, camera);

        batch = new SpriteBatch();

        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);

        backgroundBlurTexture = new Texture("background-blur.png");
        stage.addActor(new Image(backgroundBlurTexture));

        titleTexture = new Texture("title.png");
        title = new Image(titleTexture);
        title.setBounds(0, camera.viewportHeight - titleTexture.getHeight(), titleTexture.getWidth(), titleTexture
                .getHeight());
        stage.addActor(title);

        int buttonCounter = 1;
        final int BUTTON_SPACING = 80;

        Styles.addButton("Play", 20, (int) (camera.viewportHeight - title.getHeight() - (buttonCounter++ *
                BUTTON_SPACING)), 256, 64, new Runnable() {
            @Override
            public void run() {
                Voidwalker.getInstance().setScreen(new InGame());
            }
        }, stage);

        Styles.addButton("Set to 1080p", 20, (int) (camera.viewportHeight - title.getHeight() - (buttonCounter++ *
                BUTTON_SPACING)), 256, 64, new Runnable() {
            @Override
            public void run() {
                Gdx.graphics.setWindowedMode(1920, 1080);
            }
        }, stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);

        stage.draw();
    }

    @Override
    public void dispose() {
        System.out.println("DISPOSING");
        batch.dispose();
        stage.dispose();
        titleTexture.dispose();
        backgroundBlurTexture.dispose();
    }

    @Override
    public void resize(int width, int height) {
        System.out.println("Resize");
        dispose();
        show();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }
}

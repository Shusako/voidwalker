package info.shusako.voidwalker.display.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Stage;
import info.shusako.engine.ai.camera.DualFollowingCameraAI;
import info.shusako.engine.ai.camera.FollowingCameraKernel;
import info.shusako.engine.util.Timer;
import info.shusako.voidwalker.world.WorldHandler;

/**
 * Created by Shusako on 9/4/2016.
 * For project Voidwalker, 2016
 */
public class InGame implements Screen {

    /**
     * How many blocks in width the screen is when in it's most base form.
     * It's a float to avoid integer division when unexpected.
     */
    public static final float SCREEN_WIDTH_IN_METERS_BASE = 25;

    private Stage stage;
    private SpriteBatch batch;

    private FollowingCameraKernel followingCamera;
    private OrthographicCamera primaryCamera;
    private OrthographicCamera secondaryCamera;

    private Matrix4 defaultProtectionMatrix;

    public InGame() {
    }

    @Override
    public void show() {
        float aspectRatio = Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight();
        primaryCamera = new OrthographicCamera();
        primaryCamera.setToOrtho(false, SCREEN_WIDTH_IN_METERS_BASE, SCREEN_WIDTH_IN_METERS_BASE / aspectRatio);
        secondaryCamera = new OrthographicCamera();
        secondaryCamera.setToOrtho(false, SCREEN_WIDTH_IN_METERS_BASE, SCREEN_WIDTH_IN_METERS_BASE / aspectRatio);

        followingCamera = new DualFollowingCameraAI(primaryCamera, secondaryCamera);

        batch = new SpriteBatch();
        defaultProtectionMatrix = batch.getProjectionMatrix().cpy();

//        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);
//        new DialogueText(renderableManager, "The -5HP do nothing.", WorldHandler.getHandler().getMap()
//                .getAshleyHandler().getEngine().getEntities().get(0).getComponent(Box2DComponent.class), 2139999999);
//        new DialogueText("The -5HP do nothing.", WorldHandler.getHandler().getAshleyHandler().getEngine().getEntities
//                ().get(0).getComponent(Box2DComponent.class), 2139999999);
    }

    Timer timer = new Timer(200);

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.5F, 0.5F, 0.5F, 0.5F);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//
//        if(timer.hasTimePassed()) {
//            new MMOText(renderableManager, "Crips is a dick", WorldHandler.getHandler().getMap().getAshleyHandler()
//                    .getEngine().getEntities().get(0).getComponent(Box2DComponent.class), 1000);
//        }

//        WorldHandler.getHandler().render(primaryCamera);

//        stage.draw();

        WorldHandler.getHandler().update(delta);
//
        followingCamera.update();

//        batch.begin();
        followingCamera.render(batch, delta, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//        batch.end();


        /*
         * Render RenderableManager last, reset transformation to origin.
         */
//        batch.setProjectionMatrix(defaultProtectionMatrix);
//        batch.begin();
//
//        Styles.drawString(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 2, Gdx.graphics.getHeight() - 4);
//
//        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        System.out.println("RESIZE");
        dispose();
        show();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        System.out.println("DISPOSING");
        batch.dispose();
//        stage.dispose();
    }
}

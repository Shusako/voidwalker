package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Shusako on 9/12/2016.
 * For project Voidwalker, 2016
 */
public class AnimationComponent implements Component {

    public static final ComponentMapper<AnimationComponent> mapper = ComponentMapper.getFor(AnimationComponent.class);

    public Array<Animation> animations;
    public int currentAnimation;
    public float timePassed = 0;
    public float swapCounter = 0;

    /**
     * The time between frame changes in animations. Note, changing this does not affect the time, this is simply a
     * record.
     */
    public float frameTime;
}

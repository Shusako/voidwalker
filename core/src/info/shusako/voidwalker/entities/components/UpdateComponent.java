package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

/**
 * Created by Shusako on 9/19/2016.
 * For project Voidwalker, 2016
 */
public class UpdateComponent implements Component {

    public static final ComponentMapper<UpdateComponent> mapper = ComponentMapper.getFor(UpdateComponent.class);

    public Runnable runnable;
}

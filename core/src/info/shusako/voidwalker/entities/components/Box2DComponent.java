package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public class Box2DComponent implements Component {

    public static final ComponentMapper<Box2DComponent> mapper = ComponentMapper.getFor(Box2DComponent.class);

    public Body body;
}

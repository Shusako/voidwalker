package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

/**
 * Created by Shusako on 10/28/2016.
 * For project Voidwalker, 2016
 */
public class HealthComponent implements Component {

    public static final ComponentMapper<HealthComponent> mapper = ComponentMapper.getFor(HealthComponent.class);

    public float health;
    public float maxHealth;
    public float displayHeartCount;
}

package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

/**
 * Created by Shusako on 1/28/2017.
 * For project Voidwalker, 2017
 */
public class SnapToIdentifierComponent implements Component {

    public static final ComponentMapper<SnapToIdentifierComponent> mapper = ComponentMapper.getFor
            (SnapToIdentifierComponent.class);

    public int identifier;
}

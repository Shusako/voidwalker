package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

/**
 * Created by Shusako on 9/16/2016.
 * Made for project Voidwalker
 */
public class FollowingCameraComponent implements Component {

    public static final ComponentMapper<FollowingCameraComponent> mapper = ComponentMapper.getFor
            (FollowingCameraComponent.class);

    public int priority;

    public boolean canStealFocus;
    public int stealFocusDistance;

    public boolean oneTime;
    public float oneTimeDisplayTime;
}

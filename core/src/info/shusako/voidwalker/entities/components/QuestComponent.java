package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import info.shusako.voidwalker.display.renderables.QuestRenderer;
import info.shusako.voidwalker.quest.Quest;

/**
 * Created by Shusako on 12/4/2016.
 * For project Voidwalker, 2016
 */
public class QuestComponent implements Component {

    public static final ComponentMapper<QuestComponent> mapper = ComponentMapper.getFor(QuestComponent.class);

    public Quest quest;
    public boolean displayQuestMarker = true;

    public boolean inRange;

    public QuestRenderer renderable;
}

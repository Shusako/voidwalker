package info.shusako.voidwalker.entities.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public class TextureComponent implements Component {

    public static final ComponentMapper<TextureComponent> mapper = ComponentMapper.getFor(TextureComponent.class);

    public TextureRegion textureRegion;
}

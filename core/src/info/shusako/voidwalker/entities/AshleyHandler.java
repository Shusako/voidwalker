package info.shusako.voidwalker.entities;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import info.shusako.voidwalker.entities.components.UpdateComponent;
import info.shusako.voidwalker.world.WorldHandler;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 * <p>
 * Controls updating entity states and the Ashley Engine.
 */
public class AshleyHandler {

    private Engine engine = new Engine();
    private float accumulator = 0;

    public Engine getEngine() {
        return engine;
    }

    public void update(float delta) {
        updateEntities(delta);
    }

    private void updateEntities(float deltaTime) {
        accumulator += deltaTime;
        while(accumulator >= WorldHandler.TIME_STEP) {
            accumulator -= WorldHandler.TIME_STEP;

            Family family = Family.all(UpdateComponent.class).get();
            ImmutableArray<Entity> entities = engine.getEntitiesFor(family);
            for(Entity entity : entities) {
                UpdateComponent updateComponent = UpdateComponent.mapper.get(entity);
                updateComponent.runnable.run();
            }
        }
    }
}

package info.shusako.voidwalker.entities.entities.controller;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.collision.CollisionCallback;
import info.shusako.engine.input.InputGroup;
import info.shusako.engine.util.Timer;
import info.shusako.voidwalker.entities.components.AnimationComponent;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.QuestComponent;
import info.shusako.voidwalker.entities.components.SnapToIdentifierComponent;
import info.shusako.voidwalker.entities.entities.Controller;
import info.shusako.voidwalker.entities.entities.model.Player;
import info.shusako.voidwalker.world.WorldHandler;

import static info.shusako.voidwalker.entities.entities.model.Player.*;

/**
 * Created by Shusako on 11/29/2016.
 * For project Voidwalker, 2016
 */
public class PlayerController implements Controller {

    public PlayerController(Player player, Entity entity) {
        CollisionCallback collisionCallback = new CollisionCallback(player.getIdentifier() + "OnGroundSensor") {
            int collisionCount = 0;

            @Override
            public void beginContact() {
                collisionCount++;

                if(onGround == (collisionCount == 0)) {
                    onGround = true;
                    mainJump = true;
                    airJumpCounter = jumpCounter = 0;
                    onGroundSince = System.currentTimeMillis();
                }
            }

            @Override
            public void endContact() {
                collisionCount--;

                onGround = collisionCount != 0;

                if(!onGround) {
                    mainJump = false;
                }
            }
        };

        WorldHandler.getHandler().getCollisionListener().registerCollisionCallback(collisionCallback);

        this.entity = entity;
    }

    private Entity entity;

    private InputGroup inputGroup = new InputGroup("config/input.properties");

    /**
     * The timer that determines if a jump can be performed again.
     */
    private final Timer jumpTimer = new Timer(MINIMUM_TIME_MS_BETWEEN_JUMPS);
    /**
     * Whether or not the player can jump right now or not.
     */
    private boolean canJump = true;
    /**
     * The main jump, which is off of the ground.
     */
    private boolean mainJump = true;
    /**
     * The air jump number that the player is currently on.
     */
    private byte airJumpCounter = 0;
    /**
     * The number of jumps the player is on.
     */
    private byte jumpCounter = 0;
    /**
     * Whether the player bottom is colliding with something.
     */
    private boolean onGround;
    /**
     * The time that the player began being on the ground, recorded on milliseconds.
     */
    private long onGroundSince;

    /**
     * Standard updates for the entity
     */
    public void update(Box2DComponent box2DComponent) {
        Engine engine = WorldHandler.getHandler().getAshleyHandler().getEngine();
        for(Entity entity : engine.getEntitiesFor(Family.all(QuestComponent.class).get())) {
            QuestComponent component = QuestComponent.mapper.get(entity);
            Box2DComponent questHolderBox2DComponent = Box2DComponent.mapper.get(component.quest.questHolder());
            float distance = questHolderBox2DComponent.body.getPosition().dst(box2DComponent.body.getPosition());
            component.inRange = distance < component.quest.getActivateDistance();
        }
    }

    /**
     * Checks for input from input group and performs the proper actions
     */
    public void handleInput(Box2DComponent box2DComponent) {
        if(Gdx.input.isKeyPressed(inputGroup.getKeyForProperty("move_right"))) {
            move(box2DComponent, 100, 0);
        }
        if(Gdx.input.isKeyPressed(inputGroup.getKeyForProperty("move_left"))) {
            move(box2DComponent, -100, 0);
        }
        if(Gdx.input.isKeyPressed(inputGroup.getKeyForProperty("jump"))) {
            if(mainJump && canJump) {
                jump(box2DComponent);

                jumpCounter++;
                canJump = false;
            } else if(airJumpCounter < ADDITIONAL_AIR_JUMPS && jumpTimer.hasTimePassed() && canJump) {
                jump(box2DComponent);

                jumpCounter++;
                airJumpCounter++;
                canJump = false;
            }
        } else {
            canJump = true;
        }
        if(Gdx.input.isKeyPressed(inputGroup.getKeyForProperty("fastfall"))) {
            if(!onGround) {
                if(box2DComponent.body.getLinearVelocity().y > 0) {
                    Vector2 vel = box2DComponent.body.getLinearVelocity();
                    box2DComponent.body.setLinearVelocity(vel.x, 0);
                }
                box2DComponent.body.applyForceToCenter(0, -10, true);

                airJumpCounter = ADDITIONAL_AIR_JUMPS + 1; // sorry, no extra jumps after this
            }
        }
        if(Gdx.input.isKeyPressed(inputGroup.getKeyForProperty("snapToFriend"))) {
            int identifier = SnapToIdentifierComponent.mapper.get(entity).identifier;
            ImmutableArray<Entity> entities = WorldHandler.getHandler().getAshleyHandler().getEngine().getEntitiesFor
                    (Family.all(SnapToIdentifierComponent.class).get());
            for(Entity entity : entities) {
                if(entity != this.entity)
                    if(SnapToIdentifierComponent.mapper.get(entity).identifier == identifier) {
                        Box2DComponent target = Box2DComponent.mapper.get(entity);
                        Vector2 targetPosition = target.body.getPosition();
                        box2DComponent.body.setTransform(targetPosition.x + (Box2DUtility.getWidth(target) / 2),
                                targetPosition.y + (Box2DUtility.getHeight(box2DComponent) / 2) + 0.1F, 0);
                        box2DComponent.body.setActive(true);
                        box2DComponent.body.setAwake(true);
                        break;
                    }
            }
        }


        /*
         * Test keys, for debug only.
         */
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            Vector2 velocity = box2DComponent.body.getLinearVelocity();
            velocity.y = 1000;
            box2DComponent.body.setLinearVelocity(velocity);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            Vector2 velocity = box2DComponent.body.getLinearVelocity();
            velocity.y = 0;
            velocity.x = 0;
            box2DComponent.body.setLinearVelocity(velocity);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            Vector2 velocity = box2DComponent.body.getLinearVelocity();
            velocity.x = 1000;
            box2DComponent.body.setLinearVelocity(velocity);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            Vector2 velocity = box2DComponent.body.getLinearVelocity();
            velocity.x = -1000;
            box2DComponent.body.setLinearVelocity(velocity);
        }

//        if(Gdx.input.isKeyPressed(Input.Keys.R)) {
//            WorldHandler.getHandler().setMap("testLevel0");
//        }
    }

    private void jump(Box2DComponent box2DComponent) {
        float jumpHeight = JUMP_HEIGHT_IN_BLOCKS;
        int timesToDepreciateJumpHeight = jumpCounter + ((jumpCounter == airJumpCounter && !mainJump) ? 1 : 0);
        for(int i = 0; i < timesToDepreciateJumpHeight; i++) {
            jumpHeight *= JUMP_HEIGHT_FALL_OFF_PERCENTAGE;
        }

        Vector2 vel = box2DComponent.body.getLinearVelocity();
        //v^2 = v0^2 + 2a(dx)
        // v0^2 = -2a(dx)
//                vel.y = (float) Math.sqrt(-2 * WorldHandler.getHandler().getMap().getWorld().getGravity().y *
//                        JUMP_HEIGHT_IN_BLOCKS);
        vel.y = (float) Math.sqrt(-2 * WorldHandler.getHandler().getWorld().getGravity().y * jumpHeight);
        vel.x *= inAirSpeedPercentage;
        box2DComponent.body.setLinearVelocity(vel);
    }

    /**
     * Switches between player animations based on motion
     */
    public void handleAnimation(Box2DComponent box2DComponent, AnimationComponent animationComponent) {
        float xVel = box2DComponent.body.getLinearVelocity().x;
        float yVel = box2DComponent.body.getLinearVelocity().y;
        int currentAnimation = 0;

        // Run left - 0
        // Jump start left - 2
        // Move up left - 4
        // Move vertical transition left - 6
        // Move down left - 8
        // Move land left - 10

        if(onGround) {
            currentAnimation = 0;
        } else {
            currentAnimation = 2;
            animationComponent.swapCounter = 0;

            if(Math.abs(yVel) < 2) { // only slightly moving vertically
                currentAnimation = 6; // do the moving transition
            } else if(yVel < -1E-2) { // moving downwards
                currentAnimation = 8; // do the falling transition
            } else if(System.currentTimeMillis() - onGroundSince < animationComponent.animations.get(10)
                    .getAnimationDuration() * 1000) {
                /*
                 * If you have been on the ground for less time than it takes for the landing animation component to
                 * play
                 */

                currentAnimation = 10;
            }
        }

        if(xVel > 0) // if moving to the right
            currentAnimation++; // switch to the right moving animation, instead of left

        animationComponent.currentAnimation = currentAnimation;
        animationComponent.swapCounter += Gdx.graphics.getDeltaTime();
    }

    /**
     * Applies a force to the body unless they are already at maximum velocity
     *
     * @param box2DComponent The component to apply the force to
     * @param x              The force in the x direction
     * @param y              The force in the y direction
     */
    private void move(final Box2DComponent box2DComponent, float x, float y) {
        float movementMultiplier = onGround ? 1F : inAirSpeedPercentage;
        float MAX_VELOCITY = 10 * movementMultiplier;

        float xVelocity = box2DComponent.body.getLinearVelocity().x;
        float yVelocity = box2DComponent.body.getLinearVelocity().y;

        // If you're traveling too quickly AND the velocity is in the same direction you want to go
        if(Math.abs(xVelocity) > MAX_VELOCITY && (xVelocity / x) > 0) {
            x = 0;
        }
        if(Math.abs(yVelocity) > MAX_VELOCITY && (yVelocity / y) > 0) {
            y = 0;
        }

        box2DComponent.body.applyForceToCenter(x, y, true);
    }
}

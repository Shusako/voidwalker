package info.shusako.voidwalker.entities.entities.model;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.voidwalker.display.renderables.QuestRenderer;
import info.shusako.engine.entities.EntityBase;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.FollowingCameraComponent;
import info.shusako.voidwalker.entities.components.QuestComponent;
import info.shusako.voidwalker.entities.components.TextureComponent;
import info.shusako.voidwalker.quest.quests.StarterQuest;

import static info.shusako.engine.states.States.PIXELS_PER_METER;

/**
 * Created by Shusako on 12/1/2016.
 * For project Voidwalker, 2016
 */
public class RandomNPC extends EntityBase {

    @Override
    public Component[] constructComponents(Entity entity, Object... pass) {
        TextureComponent textureComponent = new TextureComponent();
        {
            textureComponent.textureRegion = new TextureRegion(new Texture(Gdx.files.internal
                    ("entities/characters/npc/npc-facing-left.png")));
        }
        Box2DComponent box2DComponent = new Box2DComponent();
        {
            box2DComponent.body = Box2DUtility.createBody((float) pass[0], (float) pass[1], BodyDef.BodyType
                    .StaticBody);

            final float NPC_HALF_WIDTH = (textureComponent.textureRegion.getRegionWidth() / PIXELS_PER_METER) / 2;
            final float NPC_HALF_HEIGHT = (textureComponent.textureRegion.getRegionHeight() / PIXELS_PER_METER) / 2;

            Box2DUtility.createCollisionBox(box2DComponent.body, "body", NPC_HALF_WIDTH, NPC_HALF_HEIGHT, 1.0F, 1.0F,
                    0.0F);
        }
        FollowingCameraComponent followingCameraComponent = new FollowingCameraComponent();
        {
            followingCameraComponent.oneTime = true;
            followingCameraComponent.oneTimeDisplayTime = 1.0F;

            followingCameraComponent.canStealFocus = true;
            followingCameraComponent.stealFocusDistance = 5;
        }
        QuestComponent questComponent = new QuestComponent();
        {
            questComponent.quest = new StarterQuest(entity);
            questComponent.renderable = new QuestRenderer(questComponent, box2DComponent);
        }

        return new Component[] { box2DComponent, textureComponent, followingCameraComponent, questComponent };
    }

    @Override
    public String getIdentifier() {
        return "randomNPC";
    }
}

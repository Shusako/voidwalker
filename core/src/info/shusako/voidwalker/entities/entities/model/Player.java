package info.shusako.voidwalker.entities.entities.model;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.Array;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.voidwalker.display.renderables.DialogueText;
import info.shusako.voidwalker.display.renderables.HealthInfo;
import info.shusako.engine.entities.EntityBase;
import info.shusako.voidwalker.entities.components.*;
import info.shusako.voidwalker.entities.entities.controller.PlayerController;
import info.shusako.voidwalker.world.collision.CollisionCategory;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public class Player extends EntityBase {

    /**
     * How high the player can jump in blocks.
     */
    public static final float JUMP_HEIGHT_IN_BLOCKS = 3.6F;
    /**
     * Fall off jump percentage
     */
    public static final float JUMP_HEIGHT_FALL_OFF_PERCENTAGE = 0.40F;
    /**
     * The minimum time before a jump can be performed again.
     */
    public static final long MINIMUM_TIME_MS_BETWEEN_JUMPS = 150;
    /**
     * The amount of control that a player will have while in the air, as well as speed deprecation on jump.
     */
    public static float inAirSpeedPercentage = 0.6F;
    /**
     * The maximum amount of jumps that a player can do while in the air.
     */
    public static final byte ADDITIONAL_AIR_JUMPS = 1;

    /**
     * @param pass Objects passed
     *
     * @return An array of components that should be attached to the entity
     */
    @Override
    public Component[] constructComponents(Entity entity, Object... pass) {
        final PlayerController controller = new PlayerController(this, entity);

        final Box2DComponent box2DComponent = new Box2DComponent();
        {
            box2DComponent.body = Box2DUtility.createBody((float) pass[0], (float) pass[1], BodyDef.BodyType
                    .DynamicBody);

            final float PLAYER_WIDTH = 1F;
            final float PLAYER_HEIGHT = 2F;

            short collisionBits = CollisionCategory.PLAYER;
            short maskedBits = CollisionCategory.WORLD;

            Box2DUtility.createCollisionBox(box2DComponent.body, getIdentifier() + "Body", PLAYER_WIDTH / 2F,
                    PLAYER_HEIGHT / 2F, 0.9F, 7.0F, 0.0F, collisionBits, maskedBits);
            /*
             * Subtracting 1E-2F from the half height of both the left and right friction-less boxes because it
             * offsets the collision layers of the bottom. When this is not done, it causes boxes to become slippery
             * after coming into contact with the top left corner of a collision box.
             */
            Box2DUtility.createCollisionBox(box2DComponent.body, getIdentifier() + "BodyLeft", PLAYER_WIDTH / 32F,
                    PLAYER_HEIGHT / 2F - 1E-2F, new Vector2(-(PLAYER_WIDTH / 2) - (PLAYER_WIDTH / 32F), 0), 0F, 0.9F,
                    0.0F, 0.0F, collisionBits, maskedBits);
            Box2DUtility.createCollisionBox(box2DComponent.body, getIdentifier() + "BodyRight", PLAYER_WIDTH / 32F,
                    PLAYER_HEIGHT / 2F - 1E-2F, new Vector2(+(PLAYER_WIDTH / 2) + (PLAYER_WIDTH / 32F), 0), 0F, 0.9F,
                    0.0F, 0.0F, collisionBits, maskedBits);
            Box2DUtility.createSensorBox(box2DComponent.body, getIdentifier() + "OnGroundSensor", (PLAYER_WIDTH / 2F)
                    + (PLAYER_WIDTH / 16F) - (PLAYER_WIDTH / 64F), PLAYER_HEIGHT / 16F, new Vector2(0, -
                    (PLAYER_HEIGHT / 2) + (PLAYER_HEIGHT / 32F)), 0F);
        }
        final AnimationComponent animationComponent = new AnimationComponent();
        {
            String directory = "entities/characters/charlie/";
            animationComponent.animations = new Array<>();
            animationComponent.frameTime = 1 / 15F;

            String[] atlasFiles = new String[] { "running_left", "running_right", "jump_start_left",
                    "jump_start_right", "move_up_left", "move_up_right", "move_vertical_transition_left",
                    "move_vertical_transition_right", "move_down_left", "move_down_right", "move_land_left",
                    "move_land_right" };

            /*
             * Load things from an array because they have to be in order
             */
            for(String atlas : atlasFiles) {
                animationComponent.animations.add(new Animation(animationComponent.frameTime, new TextureAtlas(Gdx
                        .files.internal(directory + atlas + ".atlas")).getRegions()));
            }
        }
        final UpdateComponent updateComponent = new UpdateComponent();
        {
            updateComponent.runnable = new Runnable() {
                @Override
                public void run() {
                    controller.update(box2DComponent);
                    controller.handleInput(box2DComponent);
                    controller.handleAnimation(box2DComponent, animationComponent);
                }
            };
        }

        FollowingCameraComponent followingCameraComponent = new FollowingCameraComponent();
        {
            followingCameraComponent.priority = 1;
        }

        HealthComponent healthComponent = new HealthComponent();
        {
            healthComponent.maxHealth = 20;
            healthComponent.health = healthComponent.maxHealth;
            healthComponent.displayHeartCount = 5;
            new HealthInfo(healthComponent, box2DComponent);
        }
        SnapToIdentifierComponent snapToIdentifierComponent = new SnapToIdentifierComponent();
        {
            snapToIdentifierComponent.identifier = 1;
        }

        {
            new DialogueText("The -5HP do nothing. Also this text looks like shit.", box2DComponent, 2139999999);
//            new ParticleEmitter(box2DComponent.body.getPosition(), box2DComponent.body.getLinearVelocity(),
//                    "testparticle");
            //"testparticle");
        }

        return new Component[] { box2DComponent, animationComponent, updateComponent, followingCameraComponent,
                healthComponent, snapToIdentifierComponent };
    }

    @Override
    public String getIdentifier() {
        return "player";
    }
}

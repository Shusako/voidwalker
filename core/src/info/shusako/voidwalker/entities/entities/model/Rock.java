package info.shusako.voidwalker.entities.entities.model;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import info.shusako.engine.collision.Box2DUtility;
import info.shusako.engine.entities.EntityBase;
import info.shusako.voidwalker.entities.components.Box2DComponent;
import info.shusako.voidwalker.entities.components.FollowingCameraComponent;
import info.shusako.voidwalker.entities.components.SnapToIdentifierComponent;
import info.shusako.voidwalker.entities.components.TextureComponent;
import info.shusako.voidwalker.world.collision.CollisionCategory;

/**
 * Created by Shusako on 9/9/2016.
 * For project Voidwalker, 2016
 */
public class Rock extends EntityBase {

    @Override
    public Component[] constructComponents(Entity entity, Object... pass) {
        Box2DComponent box2DComponent = new Box2DComponent();
        {
            box2DComponent.body = Box2DUtility.createBody((float) pass[0], (float) pass[1], BodyDef.BodyType
                    .DynamicBody);
            Box2DUtility.createCollisionBox(box2DComponent.body, getIdentifier() + "Body", 1F, 1F, new Vector2(1F,
                    1F), 0F, 1.0F, 1.0F, 0.05F, CollisionCategory.PLAYER, CollisionCategory.WORLD);
        }
        TextureComponent textureComponent = new TextureComponent();
        {
            //            assert false : "Rock texture component has not been handled!";
            Texture texture = new Texture(Gdx.files.internal(getIdentifier() + ".png"));
            textureComponent.textureRegion = new TextureRegion(texture, 0, 0);
        }
        FollowingCameraComponent followingCameraComponent = new FollowingCameraComponent();
        {
            followingCameraComponent.priority = 1;
        }
        SnapToIdentifierComponent snapToIdentifierComponent = new SnapToIdentifierComponent();
        {
            snapToIdentifierComponent.identifier = 1;
        }

        return new Component[] { box2DComponent, textureComponent, followingCameraComponent,
                snapToIdentifierComponent };
    }

    @Override
    public String getIdentifier() {
        return "rock";
    }
}

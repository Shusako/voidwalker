package info.shusako.voidwalker.quest;

import com.badlogic.ashley.core.Entity;

/**
 * Created by Shusako on 12/4/2016.
 * For project Voidwalker, 2016
 */
public interface Quest {

    /**
     * @return The progress on the quest, between 0.0 and 1.0, where 1.0 is complete.
     */
    float progress();

    /**
     * @return The distance for the quest holder effects to get activated
     */
    float getActivateDistance();

    /**
     * @return The quests narrative, or what should be displayed in the quest log and when accepting the quest.
     */
    String narrative();

    /**
     * @return The entity object of the quest holder.
     */
    Entity questHolder();
}

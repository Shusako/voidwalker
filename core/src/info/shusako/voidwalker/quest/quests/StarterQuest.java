package info.shusako.voidwalker.quest.quests;

import com.badlogic.ashley.core.Entity;
import info.shusako.voidwalker.quest.Quest;

/**
 * Created by Shusako on 12/4/2016.
 * For project Voidwalker, 2016
 */
public class StarterQuest implements Quest {

    public Entity entity;

    public StarterQuest(Entity entity) {
        this.entity = entity;
    }

    @Override
    public float progress() {
        return 1.0F;
    }

    @Override
    public String narrative() {
        return "This is the starter quest.\n" + "Here you will find all the information there is pertaining to the "
                + "quest you are being assigned.\n" + "If you wish to accept the quest, click the 'Accept' button " +
                "below.";
    }

    @Override
    public Entity questHolder() {
        return this.entity;
    }

    @Override
    public float getActivateDistance() {
        return 3F;
    }
}
